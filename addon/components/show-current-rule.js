import Component from '@ember/component';
import {computed, defineProperty, get} from '@ember/object';
import {htmlSafe} from '@ember/string';
import {isEmpty} from '@ember/utils';
import layout from '../templates/components/show-current-rule';

const BASE_TYPE = htmlSafe('<i>Base</i>');

/**
 * Show Current Rule is an entirely internal component that is used to display the rules in the top half of the Access Control
 * component/modal. It should not be called within the apps, and the only things that can be configured are the Access Control
 * Component is that it passes through the `readOnly` property (will not display buttons if readOnly), and it passes through the
 * `booleanTypes` in order to display them.
 *
 * Again, however, this is entirely set up for you, and thus, as long as you set up the Access Control Component correctly, this will
 * also work as expected.
 * @class ShowCurrentRule
 * @module Components
 */
const ShowCurrentRule = Component.extend({
    layout,

    tagName: 'li',

    /**
     * The Rule is the Access Control is a single row from the Access Table, which is to be displayed (includes user type, boolean types
     * and information about the user/group/dept, etc). This is displayed by the Access Control component in three groups (divided by user type).
     * @property rule
     * @type {Model}
     */
    rule: null,

    /**
     * The property is a string which is usually the User Type (User/Dept/Group), and it is displayed in bold, and used to get the name of the property.
     * @property property
     * @type {String}
     */
    property: null,
    /**
     * The Boolean Types of this rule, whether or not this user has access to each one.
     * @property types
     * @type {Object}
     */
    types: null,

    /**
     * activeTypes is an internally set (by `didReceiveAttrs`) comma separated string for each boolean type this
     * rule has access to. It is what is actually displayed. Default is "Base" if rule has no extra permissions.
     *
     * @property activeTypes
     * @private
     * @type {String}
     */
    activeTypes: BASE_TYPE,
    /**
     * Edit is the action that is passed in for the edit button.
     * @property edit
     * @type {Action}
     */
    edit: () => {},
    /**
     * Delete is the action that is passed in for the delete button.
     * @property delete
     * @type {Action}
     */
    delete: () => {},

    /**
     * readOnly is the property which determines if the buttons will be displayed to this user or not.
     * It is passed on from the Access Control Component's `readOnly` property.
     * @property readOnly
     * @type {Boolean}
     */
    readOnly: false,

    // name of the relevant department for this rule
    /**
     * Computed Name of the relevant department for this rule. Will be displayed in parenthesis if applicable.
     * @property department
     * @type {Computed String}
     * @private
     */
    department: computed('property', 'rule.dept', function() {
        if (this.get('property') !== 'dept') {
            return this.get('rule.dept');
        }
        return null;
    }),

    didReceiveAttrs() {
        this._super(...arguments);
        let types = this.get('types');
        if (!isEmpty(types)) {
            // this linter rule is dumb and does not realize I use rule
            // eslint-disable-next-line ember/require-computed-property-dependencies
            defineProperty(this, 'activeTypes', computed(`rule.${types.getEach('id').join(',')}`, function() {
                let rule = this.get('rule');
                let active = types.filter((type) => get(rule, type.id));
                if (isEmpty(active)) {
                    return BASE_TYPE;
                }
                return active.getEach('name').join(', ');
            }));
        }
    },

    actions: {
        edit() {
            this.edit(this.get('rule'));
        },

        delete() {
            this.delete(this.get('property'), this.get('rule.id'));
        }
    }
});

ShowCurrentRule.reopenClass({
    positionalParams: ['rule', 'property', 'readOnly']
});

export default ShowCurrentRule;
