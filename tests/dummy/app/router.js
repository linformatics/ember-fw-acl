import Router from '@ember/routing/router';
import config from './config/environment';

const ACLRouter = Router.extend({
    location: config.locationType,
    rootURL: config.rootURL
});

ACLRouter.map(function() {
});

export default ACLRouter;
