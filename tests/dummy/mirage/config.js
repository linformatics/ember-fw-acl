export function sessionRoutes(server) {
    server.get('/gc/session/check/', () => {
        return {
            isActive: true
        };
    });

    server.get('/gc/session/', () => {
        return {};
    });
}

export default function() {
    this.namespace = 'api';

    sessionRoutes(this);
}
