import {A as emberA} from '@ember/array';
import {computed, get} from '@ember/object';
import {alias, empty, filter, notEmpty} from '@ember/object/computed';
import Mixin from '@ember/object/mixin';
import {debounce} from '@ember/runloop';
import {inject as injectService} from '@ember/service';
import {capitalize} from '@ember/string';
import {isPresent, isBlank, isEmpty, isNone} from '@ember/utils';
import {sentenceCase} from '../helpers/sentence-case';
import {task} from 'ember-concurrency';
import RSVP from 'rsvp';
import {createErrorMessage} from '@bennerinformatics/ember-fw/utils/error';

const USER_TYPES = ['user', 'group', 'dept'];
/**
 * For a full description on how to use the Access Control Component, see our robust guide
 * [here](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-acl/concepts/access-control-client-side). This is merely an API guide on all the various aspects of the Access Control Component
 * Mixin. Note that since this is most often used as a modal, all of the manipulation you are able to do is passed through as a `model` hash. Thus, you will notice, all the non-private properties
 * described here are given the prefix `model.` because they should all be passed in using the hash for model, as described in the link above. Check "private" properties to see the documentation
 * for the internal properties, which are not to be configured from the outside.
 *
 * @class AccessControlComponentMixin
 * @module Mixins
 */
export default Mixin.create({
    accessRules: [],
    loading: true,

    /**
     * Internal array set by `didReceiveAttrs` showing the user types the current user is allowed to see/edit.
     * @property allowedTypes
     * @type {Array}
     * @private
     */
    allowedTypes: [],
    /**
     * Internal array set by `didReceiveAttrs` showing the departments the current user is allowed to see/edit.
     * @property allowedTypes
     * @type {Array}
     * @private
     */
    allowedDepts: [],

    /**
     * Computed object which contains all the boolean types with their names and descriptions (for the Permissions check boxes).
     * @property allowedTypes
     * @type {Object}
     * @private
     */
    booleanTypes: computed('allowedTypes.[]', 'model.{typeNames,typeDesc}', function() {
        let names = this.get('model.typeNames') || {};
        let descriptions = this.get('model.typeDesc') || {};
        return this.get('allowedTypes')
            .filter((type) => !USER_TYPES.includes(type))
            .map((type) => {
                let name = names[type] || sentenceCase(type);

                let desc = null;
                if (descriptions[type]) {
                    desc = descriptions[type];
                }
                return {id: type, name, desc};
            });
    }),
    /**
     * Computed object which contains only those Boolean Types, which are not found in `model.hideTypes`.
     * @property visibleBooleans
     * @type {Object}
     * @private
     */
    visibleBooleans: computed('booleanTypes.@each.id', 'model.hideTypes.[]', function() {
        let hide = this.get('model.hideTypes');
        let bools = this.get('booleanTypes');
        if (isEmpty(hide)) {
            return bools;
        }
        return bools.reject((type) => hide.includes(type.id));
    }),

    /**
     * Computed object which contains visibleBooleans by categories that as passed into `model.typeCategories`.
     * @property categorizedBooleans
     * @type {Object}
     * @private
     */
    categorizedBooleans: computed('visibleBooleans.@each.id', 'model.typeCategories.[]', function() {
        // with the typeCategories passed in, we only get the ids of the types.
        // we need to grab the actual data from visibleBooleans
        let visibleBooleans = this.get('visibleBooleans');
        return this.get('model.typeCategories').map((category) => {
            let types = category.types.map((type) => {
                return visibleBooleans.find((bool) => bool.id == type);
            });
            return {name: category.name, types};
        });
    }),

    /**
     * Rule being edited
     * @property newRule
     * @type {Object}
     * @private
     */
    newRule: {},

    /** If true, disables the edit checkbox.
     * @property forceEdit
     * @type {Boolean}
     * @private
     * @default false
    */
    forceEdit: false,

    ajax: injectService(),
    currentUser: injectService(),
    config: injectService(),
    notifications: injectService(),

    /**
     * Boolean to determine if all the Visible Boolean Types are checked (so that the button can be displayed as "Uncheck all")
     * @property allChecked
     * @type {Boolean}
     * @private
     */
    allChecked: false,
    /**
     * Department model that is selected in the optional dropdown to filter search by.
     * @property selectedDept
     * @type {Model}
     * @private
     */
    selectedDept: null,

    /**
     * Computed Boolean which determines if there is no type selected of the newRule.
     * @property noType
     * @type {Boolean}
     * @private
     */
    noType: empty('newRule.type'),
    /**
     * Computed Boolean to determine if the rule is an old rule being edited.
     * @property editing
     * @type {Boolean}
     * @private
     */
    editing: notEmpty('newRule.id'),
    /**
     * Computed Boolean to determine if the Limit Dept checkbox should be displayed.
     * @property showCurrentCheckbox
     * @type {Boolean}
     * @private
     */
    showCurrentCheckbox: computed('newRule.type.@each.type', 'editing', function() {
        return !this.get('editing') && this.get('newRule.type').length && this.get('newRule.type').every((type) => type.type === 'user');
    }),

    /* Model properties, these can be set in model passed in */

    /**
     * Resource type being edited
     * @type {String}
     * @property model.resource
     */

    /**
     * Resource ID being edited
     * @property model.id
     */

    /**
     * Name of the resource being edited
     * @type {String}
     * @property model.name
     */

    /**
     * Description of base permission
     * @type {String}
     * @property model.baseDesc
     */

    /**
     * Object of type ID to custom display name
     * @type {Object}
     * @property model.typeNames
     */

    /**
     * Object of type ID to custom display info icon
     * @type {Object}
     * @property model.typeDesc
     */

    /**
     * Any types in this array will be checked by default
     * @type {Array}
     * @property model.typeDefaults
     */

    /**
     * Defines types that depend on other types, so that those types will automatically be set
     * Expected format (if parent type is set, each of the children types will be set automatically):
     * {parentType: [childType1, childType2]}
     * @type {Object}
     * @property model.typeDependencies
     */

    /**
     * Defines categories for types. If set, modal will display types under their respective categories, rather than all together.
     * Format: [{name: "Category 1", types: [type1, type2]}, {name: "Category 2", types: [type3, type4]}]
     * Make sure all all types are within a category; any type that is not in a category will not be shown.
     * You can also put a type in multiple categories, which should work as expected although it hasn't been tested extensively.
     * @type {Array}
     * @property model.typeCategories
     */

    /**
     * Any types in this array will be hidden in the modal
     * @type {Array}
     * @property model.hideTypes
     */

    /**
     * Don't show the limit dept. checkbox, even if a user is selected
     * @type {Boolean}
     * @property model.disableLimitDept
     */

    /**
     * If true, cannot add new rules
     * @property model.readOnly
     * @type {Boolean}
     */
    readOnly: alias('model.readOnly'),
    /**
     * Array of rules that have the userType of "dept". Will be empty if currentUser's role does not allow
     * them to have access to the "dept" user type (see Access Control Permissions configuration,
     * [here](https://linformatics.bitbucket.io/docs/addons/server/access-control/config/#permissions)).
     *
     * @property deptRules
     * @type {Array}
     * @private
     */
    deptRules: filter('accessRules', function (rule) {
        return isPresent(rule.dept) && !isPresent(rule.group) && !isPresent(rule.user)
            && this.get('allowedTypes').includes('dept');
    }),
    /**
     * Array of rules that have the userType of "group". Will be empty if currentUser's role does not allow
     * them to have access to the "group" user type (see Access Control Permissions configuration,
     * [here](https://linformatics.bitbucket.io/docs/addons/server/access-control/config/#permissions)).
     *
     * @property groupRules
     * @type {Array}
     * @private
     */
    groupRules: filter('accessRules', function (rule) {
        return isPresent(rule.group) && this.get('allowedTypes').includes('group');
    }),
    /**
     * Array of rules that have the userType of "role". Will be empty if currentUser's role does not allow
     * them to have access to the "role" user type (see Access Control Permissions configuration,
     * [here](https://linformatics.bitbucket.io/docs/addons/server/access-control/config/#permissions)). **Note**:
     * currently roleRules are not fully implemented serverside, so this is currently turned off in all the apps.
     *
     * @property roleRules
     * @type {Array}
     * @private
     */
    roleRules: filter('accessRules', function (rule) {
        return isPresent(rule.role) && this.get('allowedTypes').includes('role');
    }),

    /**
     * Array of rules that have the userType of "user". Will be empty if currentUser's role does not allow
     * them to have access to the "user" user type (see Access Control Permissions configuration,
     * [here](https://linformatics.bitbucket.io/docs/addons/server/access-control/config/#permissions)).
     *
     * @property userRules
     * @type {Array}
     * @private
     */
    userRules: filter('accessRules', function (rule) {
        return isPresent(rule.user) && this.get('allowedTypes').includes('user');
    }),

    didReceiveAttrs() {
        this._super(...arguments);

        // Clear any existing rule
        this.set('newRule', this._defaultRule());

        let {resource, id} = this.get('model');

        let rulesUrl = this.get('config').formUrl('access', resource, id);
        let configUrl = this.get('config').formUrl('access', 'config');
        let deptsUrl = this.get('config').formGCUrl('departments');

        this.set('loading', true);

        return RSVP.all([
            this.get('ajax').request(configUrl),
            this.get('ajax').request(rulesUrl),
            this.get('ajax').request(deptsUrl)
        ]).then(([{types}, {rules}, {departments}]) => {
            this.setProperties({
                allowedTypes: types,
                accessRules: rules,
                allowedDepts: departments
            });
            this.set('loading', false);
        });
    },

    _search(term, resolve, reject) {
        if (isBlank(term)) {
            return resolve([]);
        }

        let searchUrl = this.get('config').formUrl('access', 'search', term);

        let data = {};
        if (this.get('selectedDept')) {
            data.dept = this.get('selectedDept.id');
        }

        return this.get('ajax').request(searchUrl, {data}).then((results) => {
            let groups = emberA();

            this.get('allowedTypes').forEach((type) => {
                if (results[type] && isPresent(results[type].results)) {
                    let options = results[type].results || [];

                    options.setEach('type', type);

                    groups.pushObject({
                        groupName: capitalize(type),
                        options: options.filter((option) => {
                            let existingRules = this.get(`${type}Rules`);

                            return !existingRules.findBy(`${type}_id`, option.id);
                        })
                    });
                }
            });

            return resolve(groups);
        }).catch(reject);
    },

    /**
     * Gets a rule with the default settings
     * @method _defaultRule
     * @return {Object} Rule
     * @private
     */
    _defaultRule() {
        let rule = {currentDept: true, type: []};
        let defaults = this.get('model.defaultChecked');
        if (!isEmpty(defaults)) {
            defaults.forEach((key) => {
                rule[key] = true;
            });
        }
        return rule;
    },
    /**
     * Task to add a new rule based on user selection (from both the dropdown and the Boolean Type checkboxes.
     * @method addMewRule
     * @private
     */
    addNewRule: task(function* () {
        if (!this.get('newRule.type')) {
            return yield RSVP.reject();
        }

        let data = this.get('newRule');
        let {currentDept, id} = data;
        let results = [];

        data.type.forEach((typeModel) => {
            let {type, id: typeId} = typeModel;
            let {resource, id: resourceId} = this.get('model');
            let rule = {
                id,
                type,
                resource,
                [type]: typeId,
                [resource]: resourceId
            };

            // for users, may add in current department
            if (currentDept && type === 'user' && isNone(id)) {
                rule.dept = this.get('currentUser.currentDept');
            }

            // set all bools
            this.get('visibleBooleans').getEach('id').reduce((rule, boolType) => {
                rule[boolType] = data[boolType] || false;
                return rule;
            }, rule);

            // force edit if editing the last rule
            if (this.get('forceEdit')) {
                rule.edit = true;
            }

            let accessRules = this.get('accessRules');
            if (isNone(id)) {
                let addUrl = this.get('config').formUrl('access');
                results.pushObject(this.get('ajax').post(addUrl, {data: {rule}}));
            } else {
                let editUrl = this.get('config').formUrl('access', id);
                results.pushObject(this.get('ajax').put(editUrl, {data: {rule}}));
                // remove the edited rule before updating it
                let oldRule = accessRules.findBy('id', id);
                if (!isNone(oldRule)) {
                    accessRules.removeObject(oldRule);
                }
            }
        });

        results = yield RSVP.all(results).catch((error) => {
            this.get('notifications').showError(createErrorMessage(error), 'access-control', true);
        });

        results.forEach((result) => {
            // if we got a rule, set the result
            if (result.rule) {
                this.get('accessRules').pushObject(result.rule);
                this.setProperties({newRule: this._defaultRule(), forceEdit: false, allChecked: false});
            }
        });
    }).drop(),

    /**
     * Method used to ensure that the last rule with edit is not removed (meaning no one could edit permissions). If there is
     * a string it will be displayed as a notification.
     *
     * @method _isLastRuleOrLastUser
     * @return {Boolean | String} errorMessage
     * @private
     */
    _isLastRuleOrLastUser(type, id) {
        let {resource, name} = this.get('model');

        if (type === 'user' && this.get('accessRules').filterBy('type', 'user').length === 1) {
            return `You must have at least one specific user rule per ${resource}. If you wish to delete this rule, `
                + `please add another user to this ${resource} first.`;
        }

        // We don't want users to remove the last rule because then the
        // resource would be orphaned
        let editRules = this.get('accessRules').filterBy('edit', true);
        let isLastEdit = this.get('allowedTypes').includes('edit') && (editRules.length === 1 && editRules.get('firstObject.id') === id);

        if (isLastEdit || this.get('accessRules.length') === 1) {
            return `You cannot remove the last${isLastEdit ? ' edit' : ''} rule from ${resource} "${name}". `
                + `Doing so would prevent anyone (including you) from ${isLastEdit ? 'editing' : 'accessing'} it. `
                + 'If you wish to delete this rule, please add another rule first or '
                + `delete the ${resource} manually.`;
        }

        return false;
    },

    actions: {
        /** Called to fetch search results */
        search(term) {
            return new RSVP.Promise((resolve, reject) => {
                debounce(this, this._search, term, resolve, reject, 200);
            });
        },

        /**
         * Called to set a boolean type checkbox value
         * @param {String}  type  Type key
         * @param {Boolean} value New type value
         */
        setType(type, value) {
            let typeDependencies = this.get('model.typeDependencies') || {};
            // check to see if we need to set dependencies
            if (type in typeDependencies && value) {
                typeDependencies[type].forEach((childType) => {
                    // recursively set the child types
                    this.send('setType', childType, true);
                });
            }
            this.set(`newRule.${type}`, value);
            // ideally this would be done with a computed property, but it's a little complicated since newRule has dynamic properties
            // this works well enough
            this.set('allChecked', this.get('visibleBooleans').every((type) => {
                return this.get(`newRule.${type.id}`);
            }));
        },

        /** Called to edit an existing rule */
        editRule(rule) {
            // determine rule type
            let type = USER_TYPES.find((check) => !isNone(get(rule, check)));
            if (isNone(type)) {
                return;
            }

            // update rule
            let newRule = {
                type: [{
                    type,
                    name: rule[type],
                    id: rule[`${type}_id`]
                }],
                id: get(rule, 'id')
            };
            // set booleans
            newRule = this.get('booleanTypes').getEach('id').reduce((newRule, boolType) => {
                newRule[boolType] = rule[boolType] || false;
                return newRule;
            }, newRule);
            this.set('newRule', newRule);
            this.set('allChecked', this.get('visibleBooleans').every((type) => {
                return this.get(`newRule.${type.id}`);
            }));

            // if this is the last edit rule, force edit checkbox
            if (rule.edit && this.get('allowedTypes').includes('edit') && this.get('accessRules').filterBy('edit').get('length') === 1) {
                this.set('forceEdit', true);
                newRule.edit = true;
            } else {
                this.set('forceEdit', false);
            }
        },

        /** Called to check all permissions */
        checkAll() {
            this.get('visibleBooleans').forEach((type) => {
                this.set(`newRule.${type.id}`, !this.get('allChecked'));
            });
            this.toggleProperty('allChecked');
        },

        /** Called to cancel editing of the current rule */
        cancel() {
            this.setProperties({newRule: this._defaultRule(), forceEdit: false});
        },

        /** Called to delete an existing rule */
        deleteRule(type, id) {
            let checkRule = this._isLastRuleOrLastUser(type, id);

            if (checkRule) {
                this.get('notifications').showError(checkRule, 'access-control');
                return;
            }

            let deleteUrl = this.get('config').formUrl('access', type, id);

            return this.get('ajax').del(deleteUrl).then(() => {
                let deletedRule = this.get('accessRules').findBy('id', id);

                this.get('accessRules').removeObject(deletedRule);
            });
        }
    }
});
