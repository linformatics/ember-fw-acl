import {helper} from '@ember/component/helper';
import {capitalize} from '@ember/string';
import {isEmpty} from '@ember/utils';

/**
 * The sentenceCase helper defines one function, which is used to convert the various Boolean Types to their proper checkmark
 * names. This helper may be helpful in contexts outside of ember-fw-acl, but it should probably be reserved to be used within those
 * apps which already use Access Control (ie it is probably not worth adding ember-fw-acl as a npm package solely for this helper).
 *
 * If you do have ember-fw-acl already, this function can be used in the controller file with the following import statement:
 *
 * ```js
 * import {sentenceCase} from '@bennerinformatics/ember-fw-acl/helpers/sentence-case';
 * ```
 *
 * Then it can be used just by calling `sentenceCase` with one parameter in the file.
 *
 * If you want to call it in handlebars, you can call it with the normal helper syntax as well with one paramenter:
 * ```hbs
 * {{sentence-case "editEntry"}} {{!-- Displays: Edit Entry --}}
 * ```
 *
 * @class SentenceCase
 * @module Helpers
 */

/**
 * Converts a string from camel case to sentence case
 * @method sentenceCase
 * @param  {string} string String to convert
 * @return {string}        Sentence case string
 */
export function sentenceCase(string) {
    if (isEmpty(string)) {
        return '';
    }
    return capitalize(string.replace(/([A-Z])/g, ' $1'));
}

export default helper(([text]) => {
    return sentenceCase(text);
});
