/* eslint-env node */
/* eslint-disable object-shorthand */
'use strict';

module.exports = {
    name: '@bennerinformatics/ember-fw-acl',
    included() {
        this._super.included.apply(this, arguments);
    }
};
