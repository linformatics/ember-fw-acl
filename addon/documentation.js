/* Below is the main documentation for the addon. This file is only for documentation, it doesn't actually do anything. */
/**
 * Ember-FW-ACL is an ember package with one primary purpose - to allow for the clientside implementation of our serverside addon,
 * Access Control, which is designed to be able to control access to specific data, and thus is much more granular than a the
 * role control provided through Group Control (and by extension Ember-FW-GC and Auth Core).
 *
 * This documentation covers the various aspects of ember-fw-acl, and gives you the API docs for how to properly use them. If you want more
 * conceptual documentation for Ember FW ACL, see our [Ember FW ACL docs](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-acl), and if
 * you need to know the serverside component of Access Control, see our [Access Control docs](https://linformatics.bitbucket.io/docs/addons/server/access-control).
 * If you know the exact name of the class you are looking for, there is a search bar on the top left corner of the screen for ease of access. Or you can always
 * click "s" to search and use the up and down arrow keys for results.
 *
 * @module Introduction
 * @main Introduction
 */

/**
 * Components defined by ember-fw-acl.
 *
 * @module Components
 * @main Components
 */

/**
 * Helpers defined by ember-fw-ac;.
 *
 * @module Helpers
 * @main Helpers
 */

/**
 * Mixins defined in ember-fw-acl.
 *
 * @module Mixins
 * @main Mixins
 */
