import Component from '@ember/component';

import AccessControlComponentMixin from '@bennerinformatics/ember-fw-acl/mixins/access-control-component';
import layout from '@bennerinformatics/ember-fw-acl/templates/components/access-control';

/**
 * Access Control Component, called as a regular component. You should not use this very often,
 * in fact, in all current cases, we always use the regular [Access Control Modal](AccessControlModal.html).
 * For information regarding the Access Control Component Mixin, [click here](AccessControlComponentMixin.html).
 *
 * @class AccessControlComponent
 * @module Component
 */
export default Component.extend(AccessControlComponentMixin, {
    layout,
    isModal: false
});
