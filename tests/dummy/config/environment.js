/* eslint-env node */
/* eslint-disable no-var, object-shorthand */

var pkg = require('../../../package.json');

module.exports = function(environment) {
    var ENV = {
        modulePrefix: 'dummy',
        environment: environment,
        rootURL: '/',
        locationType: 'auto',
        EmberENV: {
            FEATURES: {
                // Here you can enable experimental features on an ember canary build
                // e.g. 'with-controller': true
            }
        },

        APP: {
            name: 'ember-fw-acl',
            version: pkg.version,
            config: {
                name: 'ember-fw-acl',
                appId: 'ember-fw-acl',
                version: pkg.version,
                logo: 'assets/images/logo.png',
                api: 'api',
                url: '/'
            }
        }
    };

    if (environment === 'development') {
        // ENV.APP.LOG_RESOLVER = true;
        // ENV.APP.LOG_ACTIVE_GENERATION = true;
        // ENV.APP.LOG_TRANSITIONS = true;
        // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
        // ENV.APP.LOG_VIEW_LOOKUPS = true;
    }

    if (environment === 'test') {
        // Testem prefers this...
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = '#ember-testing';
    }

    if (environment === 'production') {
        ENV['ember-cli-mirage'] = {
            enabled: true
        };
    }

    return ENV;
};
