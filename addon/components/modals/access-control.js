import BaseModal from '@bennerinformatics/ember-fw/components/modals/base';

import AccessControlComponentMixin from '@bennerinformatics/ember-fw-acl/mixins/access-control-component';
import layout from '@bennerinformatics/ember-fw-acl/templates/components/access-control';

/**
 * Access Control Component, except it is called as a modal using FwFullscreenModal. For information regarding the Access Control Component
 * Mixin, [click here](AccessControlComponentMixin.html).
 *
 * @class AccessControlModal
 * @module Component
 */
export default BaseModal.extend(AccessControlComponentMixin, {
    layout,
    isModal: true,

    actions: {
        closeModal() {
            this.closeModal();
        }
    }
});
